{-# LANGUAGE OverloadedStrings #-}
module Stats where
import Control.Applicative
import Data.Bits
import Data.Char
import Data.Monoid
import Data.Word

import Data.ByteString.Char8
import Database.Redis
import Network.Discord.Types

zUpdateOpts :: ZaddOpts
zUpdateOpts = defaultZaddOpts { zaddChange = True }

zUpdate :: RedisCtx m f => ByteString -> [(Double, ByteString)] -> m (f Integer)
zUpdate key val = zaddOpts key val zUpdateOpts

timestamp :: Snowflake -> Double
timestamp (Snowflake s) = fromIntegral $ s `shift` (-22)

sigKey :: ByteString
sigKey = "bayes_bot:user_sig"

sigSqKey :: ByteString
sigSqKey = "bayes_bot:user_sig_sq"

sumKey :: ByteString
sumKey = "bayes_bot:user_sum"

lastMsgKey :: ByteString
lastMsgKey = "bayes_bot:last_msg"

avgDT :: ByteString -> Redis (Maybe Double)
avgDT key = do
  sigma <- dropError =<< zscore sigKey key
  total <- dropError =<< zscore sumKey key
  return $ sigma `justDiv` total
  where
    dropError :: Monad m => Either Reply (Maybe a) -> m (Maybe a)
    dropError (Right (Just a)) = return $ Just a
    dropError _ = return Nothing
    justDiv :: Maybe Double -> Maybe Double -> Maybe Double
    justDiv = liftA2 (/)

avgStdDev :: ByteString -> Redis (Maybe Double)
avgStdDev key = do
  sigma <- dropError =<< zscore sigSqKey key
  total <- dropError =<< zscore sumKey key
  avg <- avgDT key
  return $ stdDev sigma total avg
  where
    dropError :: Monad m => Either Reply (Maybe a) -> m (Maybe a)
    dropError (Right (Just a)) = return $ Just a
    dropError _ = return Nothing
    stdDev :: Maybe Double -> Maybe Double -> Maybe Double -> Maybe Double
    stdDev (Just n) (Just s) (Just avg) = Just . sqrt $ (n / s) - (avg ^^ 2)
    stdDev _ _ _ = Nothing

trackVocab :: ByteString -> Snowflake -> String -> Redis ()
trackVocab k c w = do
  Prelude.mapM (zincrby key 1 . normalize) $ Prelude.words w
  incrby (key <> ":total") . fromIntegral . Prelude.length $ Prelude.words w
  return ()
  where
    key = "bayes_bot:" <> k <> ":" <> (pack $ show c)
    normalize = pack . Prelude.reverse . normalize' . Prelude.reverse . normalize'
    normalize' :: String -> String
    normalize' (x:xs)
      | isAlphaNum x = x:xs
      | otherwise    = normalize' xs
    normalize' [] = []
