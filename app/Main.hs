{-# OPTIONS_GHC -fno-warn-orphans #-}
{-# LANGUAGE TypeFamilies, TypeOperators, OverloadedStrings #-}
{-# LANGUAGE FlexibleInstances, MultiParamTypeClasses #-}
module Main where

import Control.Applicative
import Control.Concurrent
import Control.Monad
import Data.Proxy
import System.Environment
import System.IO.Unsafe

import Control.Monad.IO.Class
import Data.ByteString.Char8 hiding (empty, putStr)
import Data.Hashable
import qualified Data.Text as Text
import Database.Redis hiding (Message)
import Network.Discord
import qualified Network.WebSockets (Connection())

import Stats

redisConn :: Connection
redisConn = unsafePerformIO $ do
  s <- getEnv "REDIS_SERVER"
  connect defaultConnectInfo {connectHost = s}
{-# NOINLINE redisConn #-}

connMVar :: MVar Network.WebSockets.Connection
connMVar = unsafePerformIO $ newEmptyMVar
{-# NOINLINE connMVar #-}

instance Alternative Redis where
  empty = reRedis empty
  a <|> b = reRedis $ (unRedis a) <|> (unRedis b)

instance MonadPlus Redis where
  mzero = empty
  mplus = (<|>)

instance DiscordAuth Redis where
  auth = liftIO $ do
    token <- getEnv "DISCORD_TOKEN"
    return $ Bot token
  version = return "0.4.2"
  runIO = runRedis redisConn

instance DiscordRest Redis where
  getRateLimit f = lookup' (hash f)
    where
      lookup' :: (Show a, Read b) => a -> Redis (Maybe b)
      lookup' f = do
        key <- Database.Redis.get . pack $ "bayes_bot:" ++ show f
        case key of
          Left  reply    -> mzero
          Right response ->
            case response of
              Nothing -> return Nothing
              Just a  -> return . read $ unpack a

  setRateLimit f l = void . multiExec $ do
    let key = pack $ "bayes_bot:" ++ (show $ hash f)
    Database.Redis.set key (pack $ show l)
    Database.Redis.expireat key $ toInteger l

instance DiscordGate Redis where
  data VaultKey Redis a = Key {key :: ByteString, typeHint :: Proxy a}
  type Vault Redis = VaultKey Redis

  get (Key{key = k}) = do
    key <- Database.Redis.get k
    case key of
      Left  reply    -> mzero
      Right response ->
        case response of
          Nothing -> mzero
          Just a  -> return . read $ unpack a
  put (Key{key = k}) a = void . Database.Redis.set k . pack $ show a

  sequenceKey = Key "bayes_bot:_sequence" Proxy
  storeFor = return

  connection = liftIO $ readMVar connMVar
  feed _ event = fork $ mapEvent (Proxy :: Proxy BayesApp) event <|> return ()

  run m conn = runIO $ do
    liftIO $ putMVar connMVar conn
    eventStream Create m
  fork = void . liftIO . forkIO . runIO

type BayesApp = (MessageCreateEvent :||: MessageUpdateEvent) :> (MakeStats :&&: TrackVocab)

data MakeStats

instance EventMap MakeStats Redis where
  type Domain   MakeStats = Message
  type Codomain MakeStats = ()
  
  mapEvent _ (Message{ messageId=m
                     , messageAuthor=User{userId=u, userName=name}
                     }) = do
    let time = timestamp m
    lastMsg <- zscore lastMsgKey . pack $ show u
    zUpdate lastMsgKey [(time, pack $ show u)]
    zincrby sumKey 1 . pack $ show u
    case lastMsg of
      Right (Just a) -> do
        zaddOpts sigKey   [(time - a, pack $ show u)]
          defaultZaddOpts { zaddChange    = True
                          , zaddIncrement = True }
        zaddOpts sigSqKey [((time - a) ^^ 2, pack $ show u)]
          defaultZaddOpts { zaddChange    = True
                          , zaddIncrement = True }
        return ()
      _ -> return ()
    avg  <- avgDT . pack $ show u
    dist <- avgStdDev . pack $ show u
    liftIO $ Prelude.putStrLn (  show name
                              ++ "\nAverage: " ++ show avg
                              ++ "\nStd Dev: " ++ show dist)

data TrackVocab

instance EventMap TrackVocab Redis where
  type Domain   TrackVocab = Message
  type Codomain TrackVocab = ()

  mapEvent _ (Message{ messageId = m, messageChannel = c, messageContent = msgT
                     , messageAuthor = User{userId = u, userIsBot = bot}
                     }) = do
    let msg = Text.unpack $ Text.toLower msgT
    trackVocab "channel" c msg
    when (not bot) $ trackVocab "user" u msg

main :: IO ()
main = runGateway gatewayUrl $ (return () :: Redis ())
